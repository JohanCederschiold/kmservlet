import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONObject;

import Domain.Distance;
import dataaccess.Dataaccess;



@WebServlet("/report")
public class KmReport extends HttpServlet {
	
	@Inject
	Dataaccess da;
	
	@GET
	@Produces("application/JSON")
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {

		res.setHeader("Access-Control-Allow-Origin", "*");

		PrintWriter writer = res.getWriter();

		
		List<Distance>allDistances = da.getMaxValue();
		
		JSONArray jArray = new JSONArray();
		
		for (Distance dist : allDistances) {
			JSONObject jObject = new JSONObject();
			jObject.put("Distance", dist.getKms());
			jArray.put(jObject);
		}
		
		writer.print(jArray.toString());
		writer.flush();
		writer.close();
		
	}
	
	@POST
	@Produces("text/plain")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		res.setHeader("Access-Control-Allow-Origin", "*");
		
		System.out.println("Servlet called");
		
//		res.getWriter().println(req.getParameter("distance"));
		da.registerDistance(Integer.valueOf(req.getParameter("distance")));
		
	}
	
	
	@Override
	protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
	    response.setHeader("Access-Control-Allow-Origin", "*");
	}

	
	
	
	
	
	
}
