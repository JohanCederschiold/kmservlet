package dataaccess;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import Domain.Distance;

@Stateless
public class DataaccessImplementation implements Dataaccess {
	
	@PersistenceContext(unitName="servlet")
	private EntityManager em;

	@Override
	public List<Distance> getMaxValue() {
		Query query = em.createQuery("select d from " + Distance.class.getName() + " d");
		List<Distance>results = query.getResultList();
		
		return results;
	}

	@Override
	public void registerDistance(int distance) {
		
		Distance dist = new Distance();
		dist.setKms(distance);
		em.persist(dist);

	}

}
