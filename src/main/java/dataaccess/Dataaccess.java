package dataaccess;

import java.util.List;

import Domain.Distance;

public interface Dataaccess {
	
	public List<Distance> getMaxValue();
	public void registerDistance(int distance);

}
